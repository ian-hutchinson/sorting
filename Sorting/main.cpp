//
//  main.cpp
//  Sorting
//
//  Created by Ian Hutchinson on 18/02/2016.
//  Copyright © 2016 Ian Hutchinson. All rights reserved.
//

#include <iostream>
#include <vector>

#include "BubbleSort.hpp"
#include "MergeSort.hpp"
#include "InsertionSort.hpp"

template<typename T>
void PrintVector(const std::vector<T>& vec)
{
    for (const auto& element : vec)
    {
        std::cout << element << " ";
    }
    
    std::cout << std::endl;
}

template<typename T>
void PerformSort(std::vector<T>& vec, void (*SortMethod)(std::vector<T>&))
{
    PrintVector(vec);
    SortMethod(vec);
    PrintVector(vec);
}

void ResetTestVectors(std::vector<int>& intVec, std::vector<char>& charVec)
{
    std::vector<int> defaultIntVec = { 5, 3, 1, 7, 4, 9, 6 };
    std::vector<char> defaultCharVec = { 'p', 'e', 'z', 'c', 'o', 'r', 'e' };
    
    intVec = defaultIntVec;
    charVec = defaultCharVec;
}

void GenerateRandomVectors(std::vector<int>& intVec, std::vector<char>& charVec, size_t size)
{
    intVec.clear();
    charVec.clear();
    
    for (int i = 0; i < size; ++i)
    {
        intVec.push_back(rand() % 10000);
        charVec.push_back(rand() % 'z');
    }
}

int main(int argc, const char * argv[])
{
    std::vector<int> intVec;
    std::vector<char> charVec;
    
    std::cout << "::: Bubble Sort :::\n";
    ResetTestVectors(intVec, charVec);
    PerformSort(intVec, &BubbleSort::DoSort<int>);
    PerformSort(charVec, &BubbleSort::DoSort<char>);
    
    std::cout << "\n::: Merge Sort :::\n";
    ResetTestVectors(intVec, charVec);
    PerformSort(intVec, &MergeSort::DoSort<int>);
    PerformSort(charVec, &MergeSort::DoSort<char>);
    
    std::cout << "\n::: Insertion Sort :::\n";
    ResetTestVectors(intVec, charVec);
    PerformSort(intVec, &InsertionSort::DoSort<int>);
    PerformSort(charVec, &InsertionSort::DoSort<char>);
    
    return 0;
}
