//
//  InsertionSort.hpp
//  Sorting
//
//  Created by Ian Hutchinson on 18/02/2016.
//  Copyright © 2016 Ian Hutchinson. All rights reserved.
//

#ifndef InsertionSort_hpp
#define InsertionSort_hpp

#include <vector>

class InsertionSort
{
public:
    template<typename T>
    static void DoSort(std::vector<T>& listToSort)
    {
        size_t sortedBoundary = 1;
        
        while (sortedBoundary < listToSort.size())
        {
            T unsortedElement = listToSort.at(sortedBoundary);
            
            size_t potentialIndexForThisElement = sortedBoundary;
            T elementToCheck = listToSort.at(potentialIndexForThisElement - 1);
            while(unsortedElement < elementToCheck)
            {
                listToSort.at(potentialIndexForThisElement) = listToSort.at(potentialIndexForThisElement - 1);
                --potentialIndexForThisElement;
                
                if (potentialIndexForThisElement == 0)
                {
                    break;
                }
                
                elementToCheck = listToSort.at(potentialIndexForThisElement - 1);
            }
            
            listToSort.at(potentialIndexForThisElement) = unsortedElement;
            ++sortedBoundary;
        }
    }
};

#endif /* InsertionSort_hpp */
