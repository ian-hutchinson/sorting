//
//  MergeSort.hpp
//  Sorting
//
//  Created by Ian Hutchinson on 18/02/2016.
//  Copyright © 2016 Ian Hutchinson. All rights reserved.
//

#ifndef MergeSort_hpp
#define MergeSort_hpp

#include <vector>

class MergeSort
{
public:
    template<typename T>
    static void DoSort(std::vector<T>& listToSort)
    {
        std::vector<std::vector<T> > lists;
        for (auto element : listToSort)
        {
            lists.push_back(std::vector<T>{element});
        }
        
        bool sorted = false;
        
        while (!sorted)
        {
            std::vector<std::vector<T> > sortedListsThisIteration;
            int numberOfMergesThisIteration = 0;
            
            for (int i = 0; i < lists.size() - 1; i += 2)
            {
                const auto& listA = lists.at(i);
                const auto& listB = lists.at(i+1);
                
                auto resultList = MergeLists(listA, listB);
                sortedListsThisIteration.push_back(resultList);
                numberOfMergesThisIteration++;
            }
            
            if (lists.size() % 2 != 0)
            {
                sortedListsThisIteration.push_back(lists.back());
            }
            
            lists = sortedListsThisIteration;
            if (lists.size() == 1)
            {
                sorted = true;
            }
        }
        
        listToSort = lists.at(0);
    };
    
private:
    template<typename T>
    static std::vector<T> MergeLists(const std::vector<T>& listA, const std::vector<T>& listB)
    {
        size_t combinedSize = listA.size() + listB.size();
        std::vector<T> result;
        
        int indexA = 0;
        int indexB = 0;
        
        while (result.size() != combinedSize)
        {
            // DRY fail
            if (indexA > listA.size() - 1)
            {
                result.insert(result.end(), listB.begin() + indexB, listB.end());
            }
            else if (indexB > listB.size() -1)
            {
                result.insert(result.end(), listA.begin() + indexA, listA.end());
            }
            else
            {
                if (listA[indexA] < listB[indexB])
                {
                    result.push_back(listA[indexA]);
                    indexA++;
                }
                else
                {
                    result.push_back(listB[indexB]);
                    indexB++;
                }
            }
        }
        
        return result;
    }
};

#endif /* MergeSort_hpp */
