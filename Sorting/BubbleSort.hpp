//
//  BubbleSort.hpp
//  Sorting
//
//  Created by Ian Hutchinson on 18/02/2016.
//  Copyright © 2016 Ian Hutchinson. All rights reserved.
//

#ifndef BubbleSort_hpp
#define BubbleSort_hpp

#include <vector>

class BubbleSort
{
public:
    template<typename T>
    static void DoSort(std::vector<T>& listToSort)
    {
        size_t numberOfElements = listToSort.size();
        bool sorted = false;
        
        int totalNumberOfIterations = 0;
        while (!sorted)
        {
            int numberOfSwapsThisIteration = 0;
            size_t upperBound = numberOfElements - totalNumberOfIterations - 1;
            for (int i=0; i < upperBound; ++i)
            {
                T& current = listToSort.at(i);
                T& next = listToSort.at(i+1);
     
                if (current > next)
                {
                    std::swap(current, next);
                    numberOfSwapsThisIteration++;
                }
            }
        
            if (numberOfSwapsThisIteration == 0)
            {
                sorted = true;
            }
            
            totalNumberOfIterations++;
        }
    }
};


#endif /* BubbleSort_hpp */
